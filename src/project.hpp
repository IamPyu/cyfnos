#pragma once

#include <toml.hpp>
#include <string>

class Project {
public:
  std::string name;
  std::string deps[32767];

  Project(const std::string &ref);
  ~Project();
};
