#include <string>
#include <fmt/core.h>
#include "lua.hpp"

Lua::Lua() {
  this->L = luaL_newstate();
	luaL_openlibs(this->L);
}

Lua::~Lua() {
	// fmt::println("Lua object destroyed.");
	lua_close(this->L);
}

bool Lua::EvalCode(std::string code) {
	luaL_loadstring(this->L, code.c_str());
  return lua_pcall(this->L, 0, 0, 0);
}
