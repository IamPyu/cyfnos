#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
}
#include <string>

struct Lua {
  lua_State *L;
  
  Lua();
  ~Lua();

	// Evaluate Lua code, returns true on failure.
  bool EvalCode(std::string code);
  
};
